const { Client } = require('pg');

class conexionDB {
  cliente = null;
  constructor() {
    this.cliente = new Client({
      user: 'postgres',
      host: 'localhost',
      database: 'nodejs',
      password: 'password',
      port: 8054,
    });
    this.conectarse();
  }
  async conectarse() {
    this.cliente.connect();
  }
  async query(query) {
    return await this.cliente.query(query);
  }
  async desconectarse(){
    await this.cliente.end();
  }
}

module.exports = conexionDB;
