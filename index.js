const express = require('express');
const app = new express();
const conexionDB = require('./db/db');

app.get('/', async (req, res)=>{

  const conexion = new conexionDB();
  try {
      const resultado = await conexion.query('SELECT * from tipo_producto');
      res.status(200).json(resultado.rows[0]);
  } catch (err) {
      console.log(err);
  } finally {
    conexion.desconectarse();
  };
});

app.listen(3000, ()=>{
  console.log('Servidor corriendo');
});
